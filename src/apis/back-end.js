import axios from 'axios';

export default axios.create({
  baseURL: 'https://file-converter-backend.herokuapp.com/api'
});
