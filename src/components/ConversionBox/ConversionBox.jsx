import React, { useEffect } from 'react';
import { STEP_CONVERTING, STEP_DONE } from '../../actions/types';

import ResultBox from '../ResultBox';
import './ConversionBox.css';

const colorTurnedOff = 'rgb(0, 100, 0)';
const colorTurnedOn = 'rgb(20, 255, 40)';

const clearColors = step => {
  if (step === STEP_DONE) {
    for (let i = 1; i <= 5; i++) {
      document.getElementById(`converting-light-${i}`).style.backgroundColor =
        colorTurnedOff;
    }
  }
};

const ConversionBox = props => {
  useEffect(() => {
    let blinkInterval;

    if (props.step === STEP_CONVERTING) {
      blinkInterval = setInterval(() => {
        for (let i = 1; i <= 5; i++) {
          const lightNode = document.getElementById(`converting-light-${i}`);
          lightNode.style.backgroundColor =
            lightNode.style.backgroundColor === colorTurnedOff
              ? colorTurnedOn
              : colorTurnedOff;
        }
      }, 400);
    }

    if (props.step === STEP_DONE) {
      for (let i = 1; i <= 5; i++) {
        document.getElementById(`converting-light-${i}`).style.backgroundColor =
          colorTurnedOn;
      }
    }

    return () => {
      clearColors(props.step);
      clearInterval(blinkInterval);
    };
  }, [props.step]);

  return (
    <section className="conversion-box-container">
      <div className="conversion-box-left"></div>
      <div className="conversion-box-bottom"></div>
      <section className="conversion-box">
        <div id="converting-light-1" className="conversion-box-light" />
        <div id="converting-light-2" className="conversion-box-light" />
        <div id="converting-light-3" className="conversion-box-light" />
        <div id="converting-light-4" className="conversion-box-light" />
        <div id="converting-light-5" className="conversion-box-light" />
      </section>
      {props.step === STEP_DONE ? (
        <section className="result-box">
          <ResultBox />
        </section>
      ) : (
        ''
      )}
    </section>
  );
};

export default ConversionBox;
