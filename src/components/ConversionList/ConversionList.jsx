import React from 'react';
import { connect } from 'react-redux';

import { fetchConversions } from '../../actions';

import ConversionEntry from '../ConversionEntry';
import './ConversionList.css';

class ConversionList extends React.Component {
  renderList(content) {
    return (
      <section className="conversion-list-container">
        <div className="conversion-list-title">
          <h1>Conversions</h1>
        </div>
        <ul className="conversion-list">{content}</ul>
      </section>
    );
  }

  componentDidMount() {
    this.props.fetchConversions();
  }

  render() {
    const entriesNotNull = this.props.conversion.entries !== null;
    const entriesEmpty =
      entriesNotNull && this.props.conversion.entries.length === 0;
    const entriesNotEmpty =
      entriesNotNull && this.props.conversion.entries.length > 0;

    if (entriesEmpty) {
      const noConversions = (
        <li className="conversion-entry">No conversions found</li>
      );
      return this.renderList(noConversions);
    }

    if (entriesNotEmpty) {
      const conversionEntries = this.props.conversion.entries
        .sort((a, b) => {
          const dateA = new Date(a.createdAt);
          const dateB = new Date(b.createdAt);
          return dateB.getTime() - dateA.getTime();
        })
        .map(conversion => (
          <ConversionEntry key={conversion.id} conversion={conversion} />
        ));

      return this.renderList(conversionEntries);
    }

    return <h1>Loading conversions</h1>;
  }
}

const mapStateToProps = state => {
  return { conversion: state.conversion };
};

export default connect(mapStateToProps, {
  fetchConversions
})(ConversionList);
