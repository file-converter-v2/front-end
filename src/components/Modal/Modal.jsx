import ReactDOM from 'react-dom';

import './Modal.css';

const Modal = props => {
  return ReactDOM.createPortal(
    <section className="modal-container" onClick={props.onDismiss}>
      <div className="modal" onClick={e => e.stopPropagation()}>
        <div className="modal-header">
          <h1>{props.title}</h1>
        </div>
        <div className="modal-content">{props.content}</div>
        <div className="modal-actions">{props.actions}</div>
      </div>
    </section>,
    document.querySelector('#modal')
  );
};

export default Modal;
