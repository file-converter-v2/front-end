import React from 'react';
import { FcImageFile } from 'react-icons/fc';
import {
  STEP_CONVERT,
  STEP_CONVERTING,
  STEP_DONE,
  STEP_PROCESSING,
  STEP_SELECT_FILE,
  STEP_SELECT_FORMAT
} from '../../actions/types';

import './ProgressBubble.css';

const getStepNumber = step => {
  switch (step) {
    case STEP_SELECT_FILE:
      return '1';
    case STEP_SELECT_FORMAT:
      return '2';
    case STEP_CONVERT:
      return '3';
    case STEP_PROCESSING:
    case STEP_CONVERTING:
    case STEP_DONE:
    default:
      return ' ';
  }
};

const getStepClass = step => {
  switch (step) {
    case STEP_SELECT_FILE:
      return 'bubble-select-file';
    case STEP_SELECT_FORMAT:
      return 'bubble-select-format';
    case STEP_CONVERT:
      return 'bubble-convert';
    case STEP_PROCESSING:
      return 'bubble-processing';
    case STEP_CONVERTING:
    case STEP_DONE:
    default:
      return 'bubble-hidden';
  }
};

const getFileClass = step => {
  switch (step) {
    case STEP_PROCESSING:
      return 'image-file-move';
    case STEP_SELECT_FILE:
    case STEP_SELECT_FORMAT:
    case STEP_CONVERT:
    case STEP_CONVERTING:
    case STEP_DONE:
    default:
      return ' ';
  }
};

const ProgressBubble = props => {
  const stepNumber = getStepNumber(props.step);
  const stepClass = getStepClass(props.step);
  const fileClass = getFileClass(props.step);

  return (
    <div>
      <div className={`bubble ${stepClass}`}>{stepNumber}</div>
      <FcImageFile className={`image-file ${fileClass}`} alt="" />
    </div>
  );
};

export default ProgressBubble;
