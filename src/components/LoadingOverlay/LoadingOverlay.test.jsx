import { render, screen } from '@testing-library/react';
import LoadingOverlay from './LoadingOverlay';

test('renders loading slider text', () => {
  render(<LoadingOverlay />);
  const linkElement = screen.getByText(/Connecting to server.../i);
  expect(linkElement).toBeInTheDocument();
});
