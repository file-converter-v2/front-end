import React from 'react';
import ReactDOM from 'react-dom';

import LoadingSquare from '../LoadingSquare';
import './LoadingOverlay.css';

const LoadingOverlay = () => {
  return ReactDOM.createPortal(
    <section className="loading-overlay">
      <h2>Connecting to server...</h2>
      <LoadingSquare />
    </section>,
    document.querySelector('#loading-overlay')
  );
};

export default LoadingOverlay;
