import React from 'react';
import { connect } from 'react-redux';
import {
  fetchConversions,
  fetchConversion,
  deleteConversion
} from '../../actions';

import { FcImageFile, FcRemoveImage } from 'react-icons/fc';

import Modal from '../Modal';
import './ConversionEntry.css';

class ConversionEntry extends React.Component {
  state = { showModal: false, fileName: '' };

  componentDidMount() {
    this.setState({
      fileName: `${
        this.props.conversion.fileName
      }.${this.props.conversion.newFormat.toLowerCase()}`
    });
  }

  handleFetchConversion = async () => {
    await this.props.fetchConversion(this.props.conversion.fileId);
    window.open(this.props.selectedLink, '_self');
  };

  handleDeleteConversion = async () => {
    await this.props.deleteConversion(this.props.conversion.fileId);
    this.props.fetchConversions();
    this.setState({ showModal: false });
  };

  handleDeleteCancel = () => this.setState({ showModal: false });
  handleDeleteButtonClick = () => this.setState({ showModal: true });

  renderModalContent() {
    if (this.state.fileName.length === 0) {
      return `Are you sure you want to delete this conversion?`;
    }

    return `Are you sure you want to delete ${this.state.fileName}?`;
  }

  renderModalActions() {
    return (
      <React.Fragment>
        <button className="button-delete" onClick={this.handleDeleteConversion}>
          Delete
        </button>
        <button onClick={this.handleDeleteCancel}>Cancel</button>
      </React.Fragment>
    );
  }

  render() {
    const creationDate = new Date(this.props.conversion.createdAt);
    const deletionModal = (
      <Modal
        title="Delete"
        content={this.renderModalContent()}
        actions={this.renderModalActions()}
      />
    );

    return (
      <li className="conversion-entry">
        <section className="conversion-entry-info">
          <span className="conversion-entry-file">{this.state.fileName}</span>
          <span className="conversion-entry-date">
            {creationDate.toLocaleDateString()} -{' '}
            {creationDate.toLocaleTimeString()}
          </span>
        </section>

        <section className="conversion-entry-tools">
          <span
            title={`Download ${this.state.fileName}`}
            onClick={this.handleFetchConversion}
          >
            <FcImageFile />
          </span>
          <span
            title={`Delete ${this.state.fileName}`}
            onClick={this.handleDeleteButtonClick}
          >
            <FcRemoveImage />
          </span>
        </section>

        {this.state.showModal ? deletionModal : ''}
      </li>
    );
  }
}

const mapStateToProps = state => {
  return { selectedLink: state.conversion.selectedLink };
};

export default connect(mapStateToProps, {
  fetchConversions,
  fetchConversion,
  deleteConversion
})(ConversionEntry);
