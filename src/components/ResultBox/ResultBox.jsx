import React, { useState } from 'react';
import { FcImageFile } from 'react-icons/fc';
import { connect } from 'react-redux';

import './ResultBox.css';

const ResultBox = props => {
  const [jiggle, setJiggle] = useState(false);
  const [open, setOpen] = useState(false);

  const handleBoxClick = () => {
    if (!open) {
      setJiggle(false);
      setOpen(true);
    } else {
      window.open(props.currentLink, '_self');
    }
  };

  setTimeout(() => {
    setJiggle(true);
  }, 600);

  return (
    <div
      className={`box ${jiggle && !open ? 'box-jiggle' : ''}`}
      onClick={handleBoxClick}
    >
      <div className="side-inner front"></div>
      <div className="side-inner back"></div>
      <div className="side-inner right"></div>
      <div className="side-inner left"></div>

      <div className="side-outer front"></div>
      <div className="side-outer back"></div>
      <div className="side-outer right"></div>
      <div className="side-outer left"></div>
      <div className="side-outer bottom"></div>

      <div
        className={`side-half ${open ? 'side-top-left-open' : 'side-top-left'}`}
      ></div>
      <div
        className={`side-half ${
          open ? 'side-top-right-open' : 'side-top-right'
        }`}
      ></div>
      <FcImageFile className={`file-icon ${open ? 'file-icon-bounce' : ''}`} />
    </div>
  );
};

const mapStateToProps = state => {
  return { currentLink: state.conversion.currentLink };
};

export default connect(mapStateToProps, null)(ResultBox);
