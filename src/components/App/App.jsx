import React from 'react';
import { connect } from 'react-redux';
import { checkServer } from '../../actions';
import ConversionBox from '../ConversionBox';
import ConversionForm from '../ConversionForm';
import ConversionList from '../ConversionList';
import LoadingOverlay from '../LoadingOverlay';
import ProcessingBox from '../ProcessingBox';

import './App.css';

class App extends React.Component {
  componentDidMount() {
    this.props.checkServer();
  }

  render() {
    return this.props.format?.isGloballyLoading ? (
      <LoadingOverlay />
    ) : (
      <section className="app-container">
        <section className="app-conversion-form">
          <ConversionForm />
        </section>
        <section className="app-conversion-boxes">
          <ProcessingBox step={this.props.conversion.currentStep} />
          <ConversionBox step={this.props.conversion.currentStep} />
        </section>
        <section className="app-conversion-list">
          <ConversionList />
        </section>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return { format: state.format, conversion: state.conversion };
};

export default connect(mapStateToProps, {
  checkServer
})(App);
