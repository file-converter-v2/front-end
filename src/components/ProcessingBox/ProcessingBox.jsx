import React from 'react';
import {
  STEP_CONVERTING,
  STEP_DONE,
  STEP_PROCESSING
} from '../../actions/types';

import paperPlane from '../../assets/paper-plane.png';
import './ProcessingBox.css';

const ProcessingBox = props => {
  const animateConversion =
    props.step === STEP_PROCESSING ||
    props.step === STEP_CONVERTING ||
    props.step === STEP_DONE;

  const lights = [1, 2, 3].map(value => {
    return (
      <div
        className={`processing-box-light ${
          animateConversion ? 'processing-blink-light-' + value : ''
        }`}
        key={value}
      ></div>
    );
  });

  return (
    <section className="processing-box-container">
      <div className="processing-box-bottom"></div>
      <div className="processing-box-right"></div>
      <section className="processing-box">{lights}</section>
      <img
        className={`processing-paper-plane ${
          animateConversion ? 'plane-fly' : ''
        }`}
        src={paperPlane}
        alt=""
      />
    </section>
  );
};

export default ProcessingBox;
