import React from 'react';

import './LoadingSquare.css';

const LoadingSquare = () => {
  return (
    <div className="loading-square">
      <section className="loading-circle-inner sliding"></section>

      <section className="loading-rectangle-inner piece top-left"></section>
      <section className="loading-rectangle-inner piece left-vert"></section>
      <section className="loading-rectangle-inner piece bottom-left"></section>

      <section className="loading-rectangle-inner piece top-right"></section>
      <section className="loading-rectangle-inner piece right-vert"></section>
      <section className="loading-rectangle-inner piece bottom-right"></section>
    </div>
  );
};

export default LoadingSquare;
