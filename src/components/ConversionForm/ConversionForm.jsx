import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import {
  fetchFormats,
  fetchConversions,
  createConversion,
  setStepSelectFile,
  setStepSelectFormat,
  setStepConvert,
  setStepProcessing,
  setStepConverting,
  setStepDone
} from '../../actions';
import {
  STEP_CONVERT,
  STEP_CONVERTING,
  STEP_DONE,
  STEP_PROCESSING,
  STEP_SELECT_FILE,
  STEP_SELECT_FORMAT
} from '../../actions/types';
import ProgressBubble from '../ProgressBubble';

import './ConversionForm.css';

class ConversionForm extends React.Component {
  state = { selectedFilename: '' };
  formRef = React.createRef();

  onReset = () => {
    this.props.setStepSelectFile();
    this.formRef.current.reset();
  };

  onSubmit = event => {
    event.preventDefault();

    const file = event.target[0].files[0];
    const format = event.target[1].value;

    if (this.props.conversion.currentStep !== STEP_PROCESSING) {
      this.props.setStepProcessing();
    }

    setTimeout(async () => {
      if (this.props.conversion.currentStep !== STEP_CONVERTING) {
        this.props.setStepConverting();
      }

      await this.props.createConversion(file, format);

      if (this.props.conversion.currentStep !== STEP_DONE) {
        this.props.setStepDone();
      }

      this.props.fetchConversions();
    }, 4500);
  };

  onChangeSelect = () => {
    if (this.props.conversion.currentStep !== STEP_CONVERT) {
      this.props.setStepConvert();
    }
  };

  renderFormatSelect = event => {
    const selectedFileFormat = this.state.selectedFilename?.split('.')[1];
    const selectOptions = this.props.format.entries.map(entry => {
      return selectedFileFormat !== entry.name.toLowerCase() ? (
        <option key={entry.id} value={entry.name}>
          {entry.name}
        </option>
      ) : (
        ''
      );
    });

    return (
      <section>
        <select
          onChange={this.onChangeSelect}
          defaultValue="Select format"
          disabled={event.disabled}
          ref={this.formatRef}
        >
          <option disabled hidden>
            Select format
          </option>
          {selectOptions}
        </select>
      </section>
    );
  };

  onChangeFileInput = event => {
    this.setState({ selectedFilename: event.target?.files[0]?.name });
    if (this.props.conversion.currentStep !== STEP_SELECT_FORMAT) {
      this.props.setStepSelectFormat();
    }
  };

  renderFileInput = event => {
    const mimeTypes = this.props.format.entries
      .map(entry => entry.type)
      .join(', ');

    return (
      <section>
        <label
          htmlFor="file-upload"
          className={`file-upload-label ${
            event.disabled ? 'label-disabled' : ''
          }`}
        >
          Select a file
        </label>
        <input
          id="file-upload"
          type="file"
          accept={mimeTypes}
          onChange={this.onChangeFileInput}
          disabled={event.disabled}
          ref={this.fileRef}
        />
      </section>
    );
  };

  componentDidMount() {
    this.props.fetchFormats();
  }

  render() {
    if (!this.props.format.entries.length) {
      return <h1>Loading form...</h1>;
    }

    const fileInputDisabled =
      this.props.conversion.currentStep === STEP_PROCESSING ||
      this.props.conversion.currentStep === STEP_CONVERTING ||
      this.props.conversion.currentStep === STEP_DONE;
    const selectDisabled =
      this.props.conversion.currentStep === STEP_SELECT_FILE ||
      this.props.conversion.currentStep === STEP_PROCESSING ||
      this.props.conversion.currentStep === STEP_CONVERTING ||
      this.props.conversion.currentStep === STEP_DONE;
    const submitDisabled =
      this.props.conversion.currentStep === STEP_SELECT_FILE ||
      this.props.conversion.currentStep === STEP_SELECT_FORMAT ||
      this.props.conversion.currentStep === STEP_PROCESSING ||
      this.props.conversion.currentStep === STEP_CONVERTING ||
      this.props.conversion.currentStep === STEP_DONE;

    return (
      <section>
        <form className="app-form" onSubmit={this.onSubmit} ref={this.formRef}>
          <aside className="app-form-bubble">
            <ProgressBubble step={this.props.conversion.currentStep} />
          </aside>

          <section className="app-form-content">
            <div className="app-form-file">
              <Field
                name="file"
                component={this.renderFileInput}
                disabled={fileInputDisabled}
              />
            </div>

            <div>
              <Field
                name="format"
                component={this.renderFormatSelect}
                disabled={selectDisabled}
              />
            </div>

            <div>
              <button type="submit" disabled={submitDisabled}>
                Convert
              </button>
            </div>
          </section>
        </form>
        {this.props.conversion.currentStep === STEP_DONE ? (
          <button className="form-reset-button" onClick={() => this.onReset()}>
            New File
          </button>
        ) : (
          ''
        )}
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    format: state.format,
    conversion: state.conversion
  };
};

const DecoratedComponent = connect(mapStateToProps, {
  fetchFormats,
  fetchConversions,
  createConversion,
  setStepSelectFile,
  setStepSelectFormat,
  setStepConvert,
  setStepProcessing,
  setStepConverting,
  setStepDone
})(ConversionForm);

export default reduxForm({
  form: 'conversionForm'
})(DecoratedComponent);
