import backEnd from '../apis/back-end';
import {
  FETCH_CONVERSIONS,
  FETCH_CONVERSION,
  CREATE_CONVERSION,
  DELETE_CONVERSION,
  STEP_SELECT_FILE,
  STEP_SELECT_FORMAT,
  STEP_CONVERT,
  STEP_PROCESSING,
  STEP_CONVERTING,
  STEP_DONE
} from './types';

// START --- request actions

export const fetchConversions = () => async dispatch => {
  const response = await backEnd.get('/conversions');
  dispatch({ type: FETCH_CONVERSIONS, payload: response.data });
};

export const fetchConversion = uuid => async dispatch => {
  const response = await backEnd.get('/conversions/download', {
    params: {
      uuid
    }
  });
  dispatch({ type: FETCH_CONVERSION, payload: response.data });
};

export const createConversion = (file, format) => async dispatch => {
  const formData = new FormData();
  formData.append('file', file);

  const response = await backEnd.post('/conversions', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    },
    params: {
      format
    }
  });
  dispatch({ type: CREATE_CONVERSION, payload: response.data });
};

export const deleteConversion = uuid => async dispatch => {
  await backEnd.delete('/conversions', {
    params: {
      uuid
    }
  });
  dispatch({ type: DELETE_CONVERSION, payload: uuid });
};

// END --- request actions

// START --- form step actions

export const setStepSelectFile = () => async dispatch =>
  dispatch({ type: STEP_SELECT_FILE });
export const setStepSelectFormat = () => async dispatch =>
  dispatch({ type: STEP_SELECT_FORMAT });
export const setStepConvert = () => async dispatch =>
  dispatch({ type: STEP_CONVERT });
export const setStepProcessing = () => async dispatch =>
  dispatch({ type: STEP_PROCESSING });
export const setStepConverting = () => async dispatch =>
  dispatch({ type: STEP_CONVERTING });
export const setStepDone = () => async dispatch =>
  dispatch({ type: STEP_DONE });

// END --- form step actions
