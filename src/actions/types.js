// general request types
export const FETCH_FORMATS = 'FETCH_FORMATS';
export const FETCH_CONVERSIONS = 'FETCH_CONVERSIONS';
export const FETCH_CONVERSION = 'FETCH_CONVERSION';
export const CREATE_CONVERSION = 'CREATE_CONVERSION';
export const DELETE_CONVERSION = 'DELETE_CONVERSION';

// type for checking if server is alive
export const CHECK_SERVER = 'CHECK_SERVER';

// types for conversion form steps
export const STEP_SELECT_FILE = 'STEP_SELECT_FILE';
export const STEP_SELECT_FORMAT = 'STEP_SELECT_FORMAT';
export const STEP_CONVERT = 'STEP_CONVERT';
export const STEP_PROCESSING = 'STEP_PROCESSING';
export const STEP_CONVERTING = 'STEP_CONVERTING';
export const STEP_DONE = 'STEP_DONE';
