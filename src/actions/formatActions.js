import backEnd from '../apis/back-end';
import { CHECK_SERVER, FETCH_FORMATS } from './types';

export const fetchFormats = () => async dispatch => {
  const response = await backEnd.get('/formats');
  dispatch({ type: FETCH_FORMATS, payload: response.data });
};

export const checkServer = () => async dispatch => {
  dispatch({ type: CHECK_SERVER, payload: true });
  await backEnd.head('/formats');
  dispatch({ type: CHECK_SERVER, payload: false });
};
