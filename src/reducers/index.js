import { combineReducers } from 'redux';
import { reducer } from 'redux-form';
import { formatReducer } from './formatReducer';
import { conversionReducer } from './conversionReducer';

export default combineReducers({
  format: formatReducer,
  conversion: conversionReducer,
  form: reducer
});
