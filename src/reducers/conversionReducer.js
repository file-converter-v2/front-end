import {
  FETCH_CONVERSIONS,
  FETCH_CONVERSION,
  CREATE_CONVERSION,
  DELETE_CONVERSION,
  STEP_SELECT_FILE,
  STEP_SELECT_FORMAT,
  STEP_CONVERT,
  STEP_PROCESSING,
  STEP_CONVERTING,
  STEP_DONE
} from '../actions/types';

const INITIAL_STATE = {
  entries: [],
  currentLink: '',
  selectedLink: '',
  currentStep: STEP_SELECT_FILE
};

export const conversionReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    // handle conversion form steps actions
    case STEP_SELECT_FILE:
    case STEP_SELECT_FORMAT:
    case STEP_CONVERT:
    case STEP_PROCESSING:
    case STEP_CONVERTING:
    case STEP_DONE:
      return { ...state, currentStep: action.type };

    // handle request actions
    case FETCH_CONVERSIONS:
      return { ...state, entries: action.payload };
    case FETCH_CONVERSION:
      return { ...state, selectedLink: action.payload };
    case CREATE_CONVERSION:
      return { ...state, currentLink: action.payload };
    case DELETE_CONVERSION:
    default:
      return state;
  }
};
