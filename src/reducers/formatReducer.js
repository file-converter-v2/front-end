import { FETCH_FORMATS, CHECK_SERVER } from '../actions/types';

const INITIAL_STATE = {
  entries: [],
  isGloballyLoading: false
};

export const formatReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_FORMATS:
      return { ...state, entries: action.payload };
    case CHECK_SERVER:
      return { ...state, isGloballyLoading: action.payload };
    default:
      return state;
  }
};
